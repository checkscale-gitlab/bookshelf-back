package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.ArtistMapper
import bzh.lautre.bookshelf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.api.v1.model.BookWithRoleDTO
import bzh.lautre.bookshelf.api.v1.model.RoleDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.ArtistBusiness
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.specification.ArtistSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.data.domain.Page
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Api(tags = ["artist"])
class ArtistsApiImpl(
    private val business: ArtistBusiness,
    private val mapper: ArtistMapper,
    private val mapperBook: BookMapper
) : ArtistsApi {

    override fun searchArtists(
        search: String?,
        page: Long?, 
        size: Long?
    ): ResponseEntity<MutableList<ArtistDTO>> {
        val itemPage: Page<Artist> =
            this.business.search(resolveSpecificationFromInfixExpr(search ?: ""), page!!, size!!)

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/authors"),
            HttpStatus.OK
        )
    }

    override fun searchArtistsAutocomplete(
        search: String?,
        withRolesFirst: String?,
        withSeriesFirst: String?
    ): ResponseEntity<MutableList<ArtistDTO>> {
        val itemPage: Page<Artist> =
            this.business.searchWithMultipleOrder(
                search ?: "",
                0,
                5,
                withRolesFirst ?: "",
                withSeriesFirst ?: ""
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/authors"),
            HttpStatus.OK
        )
    }

    override fun getArtistById(id: Long?): ResponseEntity<ArtistDTO> {
        return ResponseEntity.of(this.business.findById(id!!).map { mapper.map(it) })
    }

    override fun updateArtist(id: Long?, artist: ArtistDTO): ResponseEntity<ArtistDTO> {

        return if (id == artist.id.toLong()) {
            ok().body(this.mapper.map(this.business.save(this.mapper.map(artist))))
        } else {
            ResponseEntity.notFound().build()
        }
    }

    override fun deleteArtist(id: Long?): ResponseEntity<Void> {
        val response: ResponseEntity<Void>
        val optional = this.business.findById(id!!)
        response = if (optional.isPresent) {
            this.business.delete(optional.get())
            if (this.business.findById(id).isPresent)
                ResponseEntity.status(500).build()
            else
                ResponseEntity.noContent().build()
        } else {
            ResponseEntity.notFound().build()
        }

        return response
    }

    override fun getArtistContracts(id: Long?): ResponseEntity<List<BookWithRoleDTO>> {
        return ok(this.business.getBooksByRole(id!!).map {
            BookWithRoleDTO()
                .role(RoleDTO().id(it.role.id).name(it.role.name))
                .books(it.books.map { book -> mapperBook.mapToMinimal(book) })
        })
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Artist> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Artist> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { ArtistSpecification(it!!) }
    }
}

