package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.BookTypeMapper
import bzh.lautre.bookshelf.api.v1.model.BookTypeDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.specification.BookTypeSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import java.util.stream.Collectors
import javax.validation.Valid

@RestController
@Api(tags = ["bookType"])
class BookTypesApiImpl(
    private val business: BookTypeBusiness,
    private val mapper: BookTypeMapper
) : BookTypesApi {

    override fun searchBookTypes(search: String?, page: Long?, size: Long?): ResponseEntity<MutableList<BookTypeDTO>> {
        val itemPage: Page<BookType> = this.business.search(resolveSpecificationFromInfixExpr(search ?: "")!!, page!!, size!!)

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<BookTypeDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/bookType"),
            HttpStatus.OK
        )
    }

    override fun searchBookTypesAutocomplete(
        search: String?
    ): ResponseEntity<MutableList<BookTypeDTO>> {
        val itemPage: Page<BookType> = this.business.search(
            resolveSpecificationFromInfixExpr(search ?: "")!!,
            0,
            5,
            Sort.Direction.DESC
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<BookTypeDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/bookType"),
            HttpStatus.OK
        )
    }

    override fun saveBookType(@Valid bookType: BookTypeDTO): ResponseEntity<BookTypeDTO> {
        val createdBookType = this.business.save(this.mapper.map(bookType))

        return ResponseEntity
                .created(ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(createdBookType.id)
                        .toUri())
                .body(this.mapper.map(createdBookType))
    }

    override fun deleteBookType(id: Long?): ResponseEntity<Void> {
        val response: ResponseEntity<Void>
        val optionalBookType = this.business.findById(id!!)
        response = if (optionalBookType.isPresent) {
            this.business.delete(optionalBookType.get())
            if (this.business.findById(id).isPresent)
                ResponseEntity.status(500).build()
            else
                ResponseEntity.noContent().build()
        } else {
            ResponseEntity.notFound().build()
        }

        return response
    }

    override fun updateBookType(id: Long?, bookType: BookTypeDTO): ResponseEntity<BookTypeDTO> {
        return if (id == bookType.id.toLong()) {
            ResponseEntity.ok().body(this.mapper.map(this.business.save(this.mapper.map(bookType))))
        } else {
            ResponseEntity.notFound().build()
        }
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<BookType>? {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<BookType> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BookTypeSpecification(it!!) }
    }
}

