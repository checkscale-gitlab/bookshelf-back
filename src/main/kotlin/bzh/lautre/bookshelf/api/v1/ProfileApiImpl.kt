package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.Series
import io.swagger.annotations.Api
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController

@RestController
@Api(tags = ["profile"])
class ProfileApiImpl(
    private val bookMapper: BookMapper,
    private val bookBusiness: BookBusiness,
    private val seriesBusiness: SeriesBusiness,
    private val bookTypeBusiness: BookTypeBusiness
) : ProfileApi {

    override fun getProfileInformation(): ResponseEntity<ProfileDTO> {
        val profile = ProfileDTO()
        val lastAddedPage = bookBusiness.getLastAdded()
        profile.lastAddedBook = PageOfMinimalBookDTO()
            .list(bookMapper.mapToMinimal(lastAddedPage.content))
            .currentPage(lastAddedPage.pageable.pageNumber.toLong())
            .totalElements(lastAddedPage.totalElements)
            .totalPages(lastAddedPage.totalPages.toLong())

        val booksCount = bookBusiness.count

        profile.booksStatInfo = StatInfoDTO()
            .total(booksCount)
            .value(booksCount - bookBusiness.unreadBookCount)

        val unfinishedSeriesPage = seriesBusiness.getUnfinishedSeries()
        profile.libraryCount = LibraryCountDTO()
            .booksCount(booksCount)
            .bookTypesCount(bookTypeBusiness.count)
            .bookTypesBooks(bookTypeBusiness.getTop3BookTypesInBooksNumber().map { KeyValueDTO().value(bookBusiness.countByBookType(it).toBigDecimal()).key(it.name) })

        profile.nextBookToRead = PageOfMinimalBookWithCoverDTO()
            .list(
                bookMapper.mapToMinimalWithCover(
                    unfinishedSeriesPage.content.map {
                        it.bookList
                            .filter { book -> book.status != BookReadStatusEnum.READ }
                            .sortedBy { book -> book.tome }[0]
                    })
            )
            .currentPage(unfinishedSeriesPage.pageable.pageNumber.toLong())
            .totalElements(unfinishedSeriesPage.totalElements)
            .totalPages(unfinishedSeriesPage.totalPages.toLong())

        return ResponseEntity.ok(profile)
    }

    override fun getProfileBookStatus(): ResponseEntity<StatInfoDTO> {
        val booksCount = bookBusiness.count

        return ResponseEntity.ok(StatInfoDTO()
            .total(booksCount)
            .value(booksCount - bookBusiness.unreadBookCount))
    }

    override fun getNextBookToRead(page: Long?, size: Long?): ResponseEntity<PageOfBookDTO> {
        val unfinishedSeriesPage: Page<Series> = seriesBusiness.getUnfinishedSeries(page!!, size!!)

        return ResponseEntity.ok(
            PageOfBookDTO()
                .list(
                    bookMapper.map(
                        unfinishedSeriesPage.content.map {
                            it.bookList
                                .filter { book -> book.status != BookReadStatusEnum.READ }
                                .sortedBy { book -> book.tome }[0]
                        })
                )
                .currentPage(unfinishedSeriesPage.pageable.pageNumber.toLong())
                .totalElements(unfinishedSeriesPage.totalElements)
                .totalPages(unfinishedSeriesPage.totalPages.toLong())
        )
    }

    override fun getLastAddedBook(page: Long?, size: Long?): ResponseEntity<PageOfBookDTO> {
        val lastAddedPage: Page<Book> = bookBusiness.getLastAdded(page!!, size!!)

        return ResponseEntity.ok(
            PageOfBookDTO()
                .list(bookMapper.map(lastAddedPage.content))
                .currentPage(lastAddedPage.pageable.pageNumber.toLong())
                .totalElements(lastAddedPage.totalElements)
                .totalPages(lastAddedPage.totalPages.toLong())
        )
    }
}

