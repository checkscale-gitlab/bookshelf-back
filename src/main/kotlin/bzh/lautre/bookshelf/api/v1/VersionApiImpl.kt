package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.VersionInformationMapper
import bzh.lautre.bookshelf.api.v1.model.VersionInformationDTO
import bzh.lautre.bookshelf.business.VersionService
import io.swagger.annotations.Api
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RestController

@Validated
@RestController
@Api(tags = ["technical"])
class VersionApiImpl(
    private val mapper: VersionInformationMapper,
    private val service: VersionService
): VersionApi {

    override fun getVersionInformation(): ResponseEntity<VersionInformationDTO> {
        return ResponseEntity.ok(mapper.map(service.getVersionInformation()))
    }
}

