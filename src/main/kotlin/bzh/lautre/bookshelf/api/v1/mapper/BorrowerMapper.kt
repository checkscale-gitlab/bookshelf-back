package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.BorrowerDTO
import bzh.lautre.bookshelf.model.Borrower
import bzh.lautre.bookshelf.model.Loan
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Named

@Mapper(
    componentModel = "spring",
)
abstract class BorrowerMapper{

    @Mapping(target = "currentLoansCount", source = "loans", qualifiedByName = ["getCurrentLoansCount"])
    abstract fun map(borrower: Borrower?): BorrowerDTO

    abstract fun map(borrower: BorrowerDTO?): Borrower

    @Named("getCurrentLoansCount")
    protected fun getCount(loans: List<Loan>): Int {
        return loans.size
    }
}
