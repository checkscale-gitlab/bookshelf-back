package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.LoanDTO
import bzh.lautre.bookshelf.api.v1.model.LoanWithBookDetailsDTO
import bzh.lautre.bookshelf.model.Loan
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(
    componentModel = "spring",
    uses = [BookMapper::class]
)
interface LoanMapper {

    @Mapping(target = "bookIsbn", source = "book.isbn")
    fun map(loan: Loan?): LoanDTO

    @Mapping(target = "book", ignore = true)
    fun mapToEntity(loan: LoanDTO?): Loan

    fun mapWithBookDetails(loan: Loan?): LoanWithBookDetailsDTO
    fun mapWithBookDetails(loans: List<Loan>?): MutableList<LoanWithBookDetailsDTO>
}
