package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.MinimalSeriesDTO
import bzh.lautre.bookshelf.api.v1.model.SeriesDTO
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.model.Series
import org.mapstruct.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Mapper(
    nullValueCheckStrategy = NullValueCheckStrategy.ON_IMPLICIT_CONVERSION,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL,
    componentModel = "spring",
    uses = [SeriesBusiness::class],
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@Component
abstract class SeriesMapper {

    @Autowired
    lateinit var seriesBusiness: SeriesBusiness

    @Autowired
    lateinit var editorBusiness: EditorBusiness

    @Transactional
    @Mappings(
        Mapping(source = "editor.name", target = "editor"),
        Mapping(source = "bookList", target = "seriesBookCount", qualifiedByName = ["getSeriesBookCount"]),
    )
    abstract fun map(series: Series): SeriesDTO

    @Mappings(
        Mapping(source = "bookList", target = "seriesBookCount", qualifiedByName = ["getSeriesBookCount"]),
    )
    abstract fun mapToMinimal(series: Series): MinimalSeriesDTO

    @Mappings(
        Mapping(target = "bookList", source = "id", qualifiedByName = ["getBookList"]),
        Mapping(target = "editor", source = "editor", qualifiedByName = ["getEditor"]),
    )
    abstract fun map(series: SeriesDTO): Series

    @Named("getBookList")
    fun getBookList(id: Long?): MutableList<Book> {
        return if( id != null) {
            this.seriesBusiness.findById(id).orElse(Series()).bookList
        } else {
            emptyList<Book>().toMutableList()
        }
    }

    @Named("getSeriesBookCount")
    fun getSeriesBookCount(bookList: MutableList<Book>): Int {
        return bookList.size
    }

    @Named("getEditor")
    fun getEditor(editor: String): Editor {
        return this.editorBusiness.getByName(editor)
    }
}
