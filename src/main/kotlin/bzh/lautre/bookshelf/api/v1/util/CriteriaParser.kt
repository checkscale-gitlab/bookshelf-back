package bzh.lautre.bookshelf.api.v1.util

import bzh.lautre.bookshelf.api.v1.util.SearchOperation.Companion.SIMPLE_OPERATION_SET
import com.google.common.base.Joiner
import lombok.extern.slf4j.Slf4j
import java.util.regex.Pattern

@Slf4j
class CriteriaParser {
    companion object {
        private val operators = Joiner.on("|").join(SIMPLE_OPERATION_SET)
        private val SpecCriteriaRegex = Pattern.compile(
            "(?<table>('?)(\\w*)(${operators})(?:(\\[)([\\w'\\-\\p{Blank}+–,]*)(\\])))|(('?)(\\w*)(${operators})(\\*)?([\\wàâçéèêëîïôûùüÿñæœ&,'\\(\\)\\-\\p{Blank}+–]*)(\\*)?)\\|?"
        )
    }

    fun parse(searchParam: String): MutableList<SpecSearchCriteria> {
        val output: MutableList<SpecSearchCriteria> = mutableListOf()
        val matcher = SpecCriteriaRegex.matcher(searchParam.replace("%2B","+"))
        while (matcher.find()) {
            output.add(if (matcher.group("table") != null) {
                SpecSearchCriteria(
                    matcher.group(2),
                    matcher.group(3),
                    matcher.group(4),
                    matcher.group(5),
                    matcher.group(6),
                    matcher.group(7)
                )
            } else {
                SpecSearchCriteria(
                    matcher.group(9),
                    matcher.group(10),
                    matcher.group(11),
                    matcher.group(12),
                    matcher.group(13),
                    matcher.group(14),
                )
            })
        }

        return output
    }
}
