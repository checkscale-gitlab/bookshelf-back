package bzh.lautre.bookshelf.api.v1.util

class SpecJoinCriteria() {

    var target: String? = null
    var key: String? = null
    var operation: SearchOperation? = null
    var value: Any? = null

    constructor(target: String?, key: String?, operation: SearchOperation?, value: Any?) : this() {
        this.key = key
        this.target = target
        this.operation = operation
        this.value = value
    }
}
