package bzh.lautre.bookshelf.api.v1.util

class SpecSearchCriteria() {

    var key: String? = null
    var operation: SearchOperation? = null
    var value: Any? = null
    var orPredicate: Boolean = false

    constructor(orPredicate: String?, key: String?, operation: SearchOperation?, value: Any?) : this() {
        this.key = key
        this.operation = operation
        this.value = value
        this.orPredicate = orPredicate != null && orPredicate == SearchOperation.OR_PREDICATE_FLAG
    }

    constructor(orPredicate: String?, key: String?, operation: String, prefix: String?, value: String?, suffix: String?): this() {
        var op = SearchOperation.getSimpleOperation(operation[0])
        if (op != null && op === SearchOperation.EQUALITY) {
            val startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX)
            val endWithAsterisk = suffix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX)
            val table = prefix != null && suffix != null && prefix.contains(SearchOperation.OPEN_TABLE) && suffix.contains(SearchOperation.CLOSE_TABLE)
            op = when {
                startWithAsterisk && endWithAsterisk -> SearchOperation.CONTAINS
                startWithAsterisk -> SearchOperation.ENDS_WITH
                endWithAsterisk -> SearchOperation.STARTS_WITH
                table -> SearchOperation.IN
                else -> op
            }
        }

        this.key = key
        this.operation = op
        this.value = value
        this.orPredicate = orPredicate != null && orPredicate == SearchOperation.OR_PREDICATE_FLAG
    }
}
