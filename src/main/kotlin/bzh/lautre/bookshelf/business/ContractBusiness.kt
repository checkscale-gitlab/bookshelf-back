package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.ContractId
import org.springframework.stereotype.Component

@Component
interface ContractBusiness: EntityBusiness<Contract, ContractId>
