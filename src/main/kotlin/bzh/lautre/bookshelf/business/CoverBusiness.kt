package bzh.lautre.bookshelf.business

import org.springframework.web.multipart.MultipartFile

interface CoverBusiness {
    fun saveCover(coverFilename: String?): String?
    fun uploadCover(file: MultipartFile, filename: String): Boolean
    fun delete(coverFilename: String?): Boolean
}
