package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Loan
import org.springframework.stereotype.Component

@Component
interface LoanBusiness: EntityBusiness<Loan, Long>