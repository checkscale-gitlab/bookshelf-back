package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Role
import org.springframework.stereotype.Component

@Component
interface RoleBusiness: EntityBusiness<Role, Long> {
    val allRoles: List<Role>
}
