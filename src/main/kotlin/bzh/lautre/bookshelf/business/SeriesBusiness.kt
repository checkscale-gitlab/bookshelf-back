package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Series
import org.springframework.data.domain.Page

interface SeriesBusiness: EntityBusiness<Series, Long> {

    val allSeries: List<Series>
    val count: Long

    fun getUnfinishedSeries(page: Long = 0, size: Long = 5): Page<Series>
}
