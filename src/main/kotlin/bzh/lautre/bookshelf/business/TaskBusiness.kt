package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Task
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import org.springframework.data.domain.Page

interface TaskBusiness: EntityBusiness<Task, Long> {
    fun getAllTasksPerStatus(status: TaskStatusEnum): List<Task>
    fun setTaskAsDone(id: Long): Boolean
    fun getPageOfLastTaskPerStatusAndType(
        type: TaskTypeEnum,
        status: TaskStatusEnum,
        page: Int,
        size: Int
    ): Page<Task>
    fun cleanAddBookTask(isbn: String)
}
