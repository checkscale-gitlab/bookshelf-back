package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.CoverBusiness
import bzh.lautre.bookshelf.business.TaskBusiness
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.repository.BookRepository
import bzh.lautre.bookshelf.specification.BookSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class BookBusinessImpl
constructor(
    private val bookRepository: BookRepository,
    private val coverBusiness: CoverBusiness,
    private val taskBusiness: TaskBusiness
) : BookBusiness {

    override fun search(specs: Specification<Book>, page: Long, size: Long, direction: Sort.Direction, sort: BookSortField): Page<Book> {
        val test = specs.and(BookSpecification.orderBooks(direction, sort))
        return this.bookRepository.findAll(
            test,
            PageRequest.of(page.toInt(), size.toInt())
        )
    }

    override fun getLastAdded(page: Long, size: Long): Page<Book> {
        return this.bookRepository.findAll(
            BookSpecification.orderBy("creationDate", false),
            PageRequest.of(page.toInt(), size.toInt())
        )
    }

    override val allBooks: List<Book>
        get() = this.bookRepository.findAll()

    override val count: Long
        get() = this.bookRepository.count()

    override val unreadBookCount: Long
        get() = bookRepository.countAllByStatusNotIn(listOf(BookReadStatusEnum.READ))

    override fun save(t: Book): Book {
        t.isbn = t.isbn?.trim()
        t.arkId = t.arkId?.trim()
        t.title = t.title?.trim()
        t.tome = t.tome
        t.year = t.year?.trim()
        t.collection = t.collection?.trim()

        t.cover = this.coverBusiness.saveCover(t.cover)

        val createdBook = bookRepository.save(t)
        taskBusiness.cleanAddBookTask(createdBook.isbn!!)
        return createdBook
    }

    override fun getByIsbnAndArkId(isbn: String, arkId: String?): Optional<Book> {
        return bookRepository.findByIsbnAndArkId(isbn, arkId)
    }

    override fun getByBookType(name: String): List<Book> {
        return this.bookRepository.findAllBySeries_BookType_Name(name)
    }

    override fun findById(id: String): Optional<Book> {
        return this.bookRepository.findById(id)
    }

    override fun getAllByIsbn(isbns: List<String>): List<Book> {
        return this.bookRepository.findAllById(isbns)
    }

    override fun getByIsbn(isbn: String): Book {
        return this.bookRepository.getOne(isbn)
    }

    override fun delete(t: Book): Boolean {
        this.bookRepository.delete(t)
        return if (this.findById(t.isbn!!).isPresent) {
            this.coverBusiness.delete(t.cover!!)
        } else {
            false
        }
    }

    override fun countByBookType(bookType: BookType): Long {
        return this.bookRepository.countBooksBySeries_BookType(bookType)
    }
}


