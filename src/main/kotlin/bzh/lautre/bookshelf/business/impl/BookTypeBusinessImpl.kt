package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.repository.BookTypeRepository
import bzh.lautre.bookshelf.specification.BookTypeSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*


@Component
class BookTypeBusinessImpl @Autowired
constructor(
    private val repository: BookTypeRepository
) : BookTypeBusiness {
    override val count: Long
        get() = this.repository.count()

    override fun getTop3BookTypesInBooksNumber(): List<BookType> {
        /*
        select bt.*, COUNT(b.isbn) from book_type bt
            join series s on bt.id = s.book_type_id
            join book b on s.id = b.series_id
        group by bt.id, bt.name
        order by COUNT(b.isbn) desc limit 3
         */
        return this.repository.findAll(BookTypeSpecification.getAllOrderByBookCount())
    }

    override val allBookTypes: List<BookType>
        get() = this.repository.findAll()

    override fun search(
        specs: Specification<BookType>,
        page: Long,
        size: Long,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<BookType> {
        return this.repository.findAll(specs, PageRequest.of(page.toInt(), size.toInt()))
    }

    override fun getByName(name: String): BookType {
        return this.repository.getBookTypeByName(name)
            .orElseGet { BookType(name) }
    }

    override fun save(t: BookType): BookType {
        t.name = t.name.trim()
        return if (t.id != null) {
            try {
                Optional.of(repository.save(t))
            } catch (e: DataIntegrityViolationException) {
                repository.getBookTypeByName(t.name)
            }
        } else {
            repository.getBookTypeByName(t.name)
        }.orElseGet { this.repository.save(t) }
    }

    override fun findById(id: Long): Optional<BookType> {
        return this.repository.findById(id)
    }

    override fun delete(t: BookType): Boolean {
        this.repository.delete(t)
        return this.findById(t.id!!).isPresent
    }
}

