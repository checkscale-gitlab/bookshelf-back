package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.ContractBusiness
import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.ContractId
import bzh.lautre.bookshelf.repository.ContractRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class ContractBusinessImpl(
    private val repository: ContractRepository
) : ContractBusiness {

    override fun save(t: Contract): Contract {
        return if (t.book.isbn !== null && t.role.id !== null) {
            repository.findById(ContractId(t.role.id,  t.book.isbn))
                .ifPresent{ t.id = it.id }
            repository.save(t)
        } else {
            repository.save(t)
        }
    }

    override fun findById(id: ContractId): Optional<Contract> {
        return repository.findById(id)
    }

    override fun deleteById(id: ContractId): Boolean {
        this.repository.deleteById(id)
        return this.findById(id).isPresent
    }
}
