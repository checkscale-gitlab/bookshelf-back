package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.CoverBusiness
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files


@Component
class CoverBusinessImpl : CoverBusiness {
    private val logger = LoggerFactory.getLogger(CoverBusinessImpl::class.java)

    @Value("\${bookshelf.covers.path}")
    private val coversPath: String? = null

    @Value("\${bookshelf.covers.search.path}")
    private val coversSearchPath: String? = null

    /**
     * Move the cover from the search folder to the final folder.
     */
    override fun saveCover(coverFilename: String?): String? {

        if (coverFilename == null || !createCoverFolder(coversPath!!)) {
            return null
        }

        val searchCover = File(coversSearchPath!! + coverFilename)
        val absolutePath = searchCover.absolutePath
        return if (searchCover.exists()) {
            val cover = File(coversPath + searchCover.name)
            try {
                if (!hasRight(searchCover)) {
                    logger.warn("The cover [{}] right are not good", absolutePath)
                    null
                } else {
                    if (cover.exists()) {
                        cover.delete()
                    }
                    if (moveFile(searchCover, cover)) {
                        logger.debug("The cover [{}] has been moved", absolutePath)
                        searchCover.name
                    } else {
                        logger.error("The cover [{}] hasn't been moved", absolutePath)
                        null
                    }
                }
            } catch (e: Exception) {
                logger.error("The cover [{}] hasn't been moved", absolutePath, e)
                null
            }
        } else {
            logger.info("The cover [{}] doesn't exist in the search folder, nothing done", absolutePath)
            coverFilename
        }
    }

    /**
     * Save in search folder the cover in param
     * */
    override fun uploadCover(file: MultipartFile, filename: String): Boolean {
        if (!createCoverFolder(coversSearchPath!!)) {
            return false
        }

        // TODO check jpg ou png !!
        val imageToSave = File("$coversSearchPath$filename.jpg")
        FileOutputStream(imageToSave).use { os -> os.write(file.bytes) }
        return imageToSave.exists()
    }


    override fun delete(coverFilename: String?): Boolean {
        return File(coversPath!! + coverFilename!!).delete()
    }

    private fun createCoverFolder(coversPath: String): Boolean {
        val directory = File(coversPath)
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                logger.error("The folder [{}] hasn't been created", directory.absolutePath)
                return false
            }

            if (!directory.setWritable(true)) {
                logger.error("The folder [{}] hasn't been set writable", directory.absolutePath)
                return false
            }

            if (!directory.setReadable(true)) {
                logger.error("The folder [{}] hasn't been set readable", directory.absolutePath)
                return false
            }
        }

        return true
    }

    private fun hasRight(file: File): Boolean {
        return file.canRead() && file.canWrite()
    }

    private fun moveFile(src: File, dst: File): Boolean {
        return Files.move(src.toPath(), dst.toPath()).toFile().exists()
    }
}
