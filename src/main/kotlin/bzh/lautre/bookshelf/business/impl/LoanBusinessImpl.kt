package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.LoanBusiness
import bzh.lautre.bookshelf.model.Loan
import bzh.lautre.bookshelf.repository.LoanRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class LoanBusinessImpl(
    private val repository: LoanRepository,
): LoanBusiness {

    override fun save(t: Loan): Loan {
        return if (t.id !== null) {
            repository.findById(t.id!!).ifPresent{ t.id = it.id }
            repository.save(t)
        } else {
            t.borrowDate = Date()
            t.borrower.loans.add(t)
            repository.save(t)
        }
    }

    override fun delete(t: Loan): Boolean {
        this.repository.delete(t)
        return this.findById(t.id!!).isPresent
    }

    override fun findById(id: Long): Optional<Loan> {
        return repository.findById(id)
    }

}