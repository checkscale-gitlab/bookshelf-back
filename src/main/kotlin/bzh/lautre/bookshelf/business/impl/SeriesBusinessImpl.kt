package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.exception.ImpossibleSeriesSave
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.repository.SeriesRepository
import bzh.lautre.bookshelf.specification.SeriesSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class SeriesBusinessImpl(
    private val seriesRepository: SeriesRepository,
    private val editorBusiness: EditorBusiness,
    private val bookTypeBusiness: BookTypeBusiness
) : SeriesBusiness {

    override fun search(
        specs: Specification<Series>,
        page: Long,
        size: Long,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Series> {
        return this.seriesRepository.findAll(
            specs.and(SeriesSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page.toInt(), size.toInt())
            }
        )
    }

    override fun findById(id: Long): Optional<Series> {
        return seriesRepository.findById(id)
    }

    override val allSeries: List<Series>
        get() = seriesRepository.findAll()

    override val count: Long
        get() = this.seriesRepository.count()


    override fun getUnfinishedSeries(page: Long, size: Long): Page<Series> {
        return this.seriesRepository.findAll(
            SeriesSpecification(
                SpecSearchCriteria(
                    null,
                    "bookStatus",
                    SearchOperation.NEGATION,
                    BookReadStatusEnum.READ
                )
            ).and(
                Specification
                    .where(
                        SeriesSpecification.distinct().and(
                            SeriesSpecification(SpecSearchCriteria()).orderBy(
                                Sort.Direction.DESC,
                                "lastReadBookDate"
                            )
                        )
                    )
            ), PageRequest.of(page.toInt(), size.toInt())
        )
    }

    override fun save(t: Series): Series {
        if (t.bookType.id == null) {
            t.bookType = this.bookTypeBusiness.save(t.bookType)
        }

        if (t.editor.id == null) {
            t.editor = this.editorBusiness.save(t.editor)
        }

        t.name = t.name.trim()
        t.displayName = t.displayName.trim()
        val optional: Optional<Series> =
            seriesRepository.findByNameAndEditorAndBookType(t.name, t.editor, t.bookType)
        return if (optional.isPresent) {
            val dbSeries = optional.get()
            if (!t.oneShot && dbSeries.oneShot && dbSeries.bookList.size > 1) {
                throw ImpossibleSeriesSave("Can't switch the series to One Shot due to the number of book on the series")
            }
            if (t.id == null) {
                t.id = dbSeries.id
            }
            seriesRepository.save(t)
        } else {
            t.bookList = mutableListOf()
            seriesRepository.save(t)
        }
    }

    override fun delete(t: Series): Boolean {
        this.seriesRepository.delete(t)
        return this.findById(t.id!!).isPresent
    }
}
