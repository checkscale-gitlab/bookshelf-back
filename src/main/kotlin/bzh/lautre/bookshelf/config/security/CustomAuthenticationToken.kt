package bzh.lautre.bookshelf.config.security

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class CustomAuthenticationToken(
    private val email: String,
    authorities: MutableCollection<out GrantedAuthority> = mutableSetOf()
) : AbstractAuthenticationToken(authorities) {
    override fun isAuthenticated(): Boolean  = true
    override fun getCredentials(): String  = this.email
    override fun getPrincipal(): String = credentials
}
