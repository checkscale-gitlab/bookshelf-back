package bzh.lautre.bookshelf.exception

open class CustomInternalErrorException(
    message: String,
    val error: String
) : Exception(message)
