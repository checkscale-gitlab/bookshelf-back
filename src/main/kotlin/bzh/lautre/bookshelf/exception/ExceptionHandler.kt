package bzh.lautre.bookshelf.exception

import bzh.lautre.bookshelf.api.v1.model.ErrorDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [CustomInternalErrorException::class])
    fun handleInternalErrors(
        exception: CustomInternalErrorException,
        request: WebRequest
    ): ResponseEntity<ErrorDTO> {
        return ResponseEntity.status(500).body(
            ErrorDTO()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.toString())
                .error(exception.error)
                .message(exception.message)
        )
    }

    @ExceptionHandler(value = [ResourceNotFound::class])
    fun handleInternalErrors(
        exception: ResourceNotFound,
        request: WebRequest
    ): ResponseEntity<ErrorDTO> {
        return ResponseEntity.status(500).body(
            ErrorDTO()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.toString())
                .error("RESOURCE_NOT_FOUND")
                .message("${exception.message}: ${exception.entity} ${exception.id}")
        )
    }
}
