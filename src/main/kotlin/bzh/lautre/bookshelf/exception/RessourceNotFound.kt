package bzh.lautre.bookshelf.exception

class ResourceNotFound(message: String, val entity: String, val id: String) : Exception(message)
