package bzh.lautre.bookshelf.liquibase.scripts

import liquibase.change.custom.CustomTaskChange
import liquibase.database.Database
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.ValidationErrors
import liquibase.resource.ResourceAccessor
import java.sql.ResultSet

class MigrateBookTypeToSeries : CustomTaskChange {

    private var resourceAccessor: ResourceAccessor? = null

    override fun getConfirmationMessage(): String {
        return "Migrate bookType from book table to series"
    }

    override fun setUp() {
    }

    override fun setFileOpener(resourceAccessor: ResourceAccessor?) {
        this.resourceAccessor = resourceAccessor
    }

    override fun validate(batabase: Database?): ValidationErrors {
        return ValidationErrors()
    }

    override fun execute(database: Database) {
        val dbConn: JdbcConnection = database.connection as JdbcConnection

        getOldData(dbConn)
            .map {
                val statement = dbConn.prepareStatement("UPDATE bookshelf.series s SET s.book_type_id = ? WHERE s.id = ?")
                statement.setLong(1, it.second)
                statement.setLong(2, it.first)
                statement
            }
            .forEach {
                println( it.toString() )
                it.executeUpdate() }
    }

    private fun getOldData(dbConn: JdbcConnection): List<Pair<Long, Long>> {
        val pairs: MutableList<Pair<Long, Long>> = mutableListOf()
        val result: ResultSet = dbConn.createStatement().executeQuery(
            "SELECT s.id as series_id, b.book_type_id " +
                    "FROM series s LEFT JOIN book b ON b.isbn = " +
                    "( SELECT b2.isbn FROM book b2 WHERE b2.series_id = s.id LIMIT 1)"
        )
        while (result.next()) {
            if (result.getLong("book_type_id") > 0) {
                pairs.add(Pair(result.getLong("series_id"), result.getLong("book_type_id")))
            }
        }
        return pairs
    }
}

