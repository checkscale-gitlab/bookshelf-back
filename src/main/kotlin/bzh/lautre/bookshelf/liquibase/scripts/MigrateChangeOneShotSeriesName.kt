package bzh.lautre.bookshelf.liquibase.scripts

import liquibase.change.custom.CustomTaskChange
import liquibase.database.Database
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.ValidationErrors
import liquibase.resource.ResourceAccessor
import java.sql.ResultSet

class MigrateChangeOneShotSeriesName : CustomTaskChange {

    private var resourceAccessor: ResourceAccessor? = null

    override fun getConfirmationMessage(): String {
        return "Migrate bookType from book table to series"
    }

    override fun setUp() {
    }

    override fun setFileOpener(resourceAccessor: ResourceAccessor?) {
        this.resourceAccessor = resourceAccessor
    }

    override fun validate(batabase: Database?): ValidationErrors {
        return ValidationErrors()
    }

    override fun execute(database: Database) {
        val dbConn: JdbcConnection = database.connection as JdbcConnection

        getOldData(dbConn)
            .map {
                val statement = dbConn.prepareStatement("UPDATE bookshelf.series s SET s.name = ?, s.display_name = ?, s.one_shot = true WHERE s.id = ?")
                statement.setString(1, it.second)
                statement.setString(2, it.second)
                statement.setLong(3, it.first)
                statement
            }
            .forEach {
                println( it.toString() )
                it.executeUpdate() }
    }

    private fun getOldData(dbConn: JdbcConnection): List<Pair<Long, String>> {
        val pairs: MutableList<Pair<Long, String>> = mutableListOf()
        val result: ResultSet = dbConn.createStatement().executeQuery(
            "SELECT s.id as series_id, b.title as book_title " +
                    "FROM series s LEFT JOIN book b ON b.series_id = s.id " +
                    "WHERE s.name like 'One-shot%'"
        )
        while (result.next()) {
            pairs.add(Pair(result.getLong("series_id"), result.getString("book_title")))
        }
        return pairs
    }
}

