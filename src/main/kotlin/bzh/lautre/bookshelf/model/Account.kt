package bzh.lautre.bookshelf.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name = "account")
class Account(
    @Id
    @Column(nullable = false, length = 100)
    var email: String? = null,
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    var role: AccountRoleEnum = AccountRoleEnum.ROLE_USER
) {
    data class Builder(
        var email: String? = null,
        var role: AccountRoleEnum = AccountRoleEnum.ROLE_USER
    ) {
        fun email(email: String) = apply { this.email = email }
        fun role(role: AccountRoleEnum) = apply { this.role = role }
        fun build() = Account(email, role)
    }
}
