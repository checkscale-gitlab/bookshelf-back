package bzh.lautre.bookshelf.model

enum class BookReadStatusEnum {
    UNREAD, READ, READING
}
