package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "contract")
class Contract(
    @ManyToOne @MapsId("roleId") var role: Role = Role(),
    @ManyToMany var artists: List<Artist> = mutableListOf(),
    @ManyToOne @MapsId("bookIsbn") var book: Book = Book(),
    @EmbeddedId var id: ContractId = ContractId(role.id, book.isbn)
): Serializable
