package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor

import javax.persistence.*


@Data
@NoArgsConstructor
@Entity(name = "Role")
@Table(name = "role")
class Role(
        @Column(unique = true) var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
        @OneToMany(mappedBy = "role", cascade = [CascadeType.ALL]) var contracts: List<Contract> = listOf()
)
