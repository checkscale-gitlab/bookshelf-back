package bzh.lautre.bookshelf.model

import lombok.Data
import java.util.*

import javax.persistence.*

@Data
@Entity
@Table(name = "task")
class Task(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    var createDate: Date = Date(),
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    var type: TaskTypeEnum = TaskTypeEnum.ADD_BOOK,
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    var status: TaskStatusEnum = TaskStatusEnum.TODO,
    @Column(unique = true)
    var extra: String = "",
)
