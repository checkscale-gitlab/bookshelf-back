package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "web_links")
class WebLinks(
        @Column var value: String? = null,
        @Enumerated(EnumType.STRING) @Column var type: WebLinksTypeEnum = WebLinksTypeEnum.OTHER,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
        @ManyToOne var artists: Artist? = null
)
