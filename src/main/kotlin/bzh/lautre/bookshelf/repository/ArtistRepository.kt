package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Artist
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface ArtistRepository : JpaRepository<Artist, Long>, JpaSpecificationExecutor<Artist>, ArtistRepositoryCustom {

    fun findByName(name: String): Optional<Artist>

}
