package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Artist
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface ArtistRepositoryCustom {
    fun getArtistWithSpecificSort(
        name: String,
        withRolesFirst: String,
        withSeriesFirst: String,
        page: Pageable
    ): Page<Artist>
}