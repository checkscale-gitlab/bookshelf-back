package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Artist
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.math.BigInteger
import javax.persistence.EntityManager


@Component
class ArtistRepositoryCustomImpl(
    private val entityManager: EntityManager
) : ArtistRepositoryCustom {
    override fun getArtistWithSpecificSort(
        name: String,
        withRolesFirst: String,
        withSeriesFirst: String,
        page: Pageable
    ): Page<Artist> {
        val query = entityManager.createNativeQuery(
            """
                select *
                from (
                         select vasrc.*,
                                row_number() OVER (
                                    partition by vasrc.artist_id 
                                    ORDER BY 
                                        IF(roles = :roles, 0, 1), 
                                        IF(series = :series, 0, 1),
                                        vasrc.count DESC,
                                        vasrc.name DESC
                                ) AS rn
                         from v_artist_series_role_count as vasrc
                     ) subquery
                         join artist on artist_id = artist.id
                WHERE artist.name like :name AND rn = 1
                ORDER BY 
                    IF(subquery.roles = :roles, 0, 1), 
                    IF(subquery.series = :series, 0, 1),
                    subquery.count DESC,
                    subquery.name DESC
            """, Artist::class.java
        )
            .setParameter("name", "%$name%")
            .setParameter("roles", withRolesFirst)
            .setParameter("series", withSeriesFirst)
        query.firstResult = page.pageNumber * page.pageSize
        query.maxResults = page.pageSize

        val countQuery = entityManager.createNativeQuery(
            """
                select count(*)
                from (
                         select vasrc.*,
                                row_number() OVER (
                                    partition by vasrc.artist_id 
                                    ORDER BY 
                                        IF(roles = :roles, 0, 1), 
                                        IF(series = :series, 0, 1),
                                        vasrc.count DESC,
                                        vasrc.name DESC
                                ) AS rn
                         from v_artist_series_role_count as vasrc
                     ) subquery
                         join artist on artist_id = artist.id
                WHERE artist.name like :name AND rn = 1
            """)
            .setParameter("name", "%$name%")
            .setParameter("roles", withRolesFirst)
            .setParameter("series", withSeriesFirst)


        return PageImpl(query.resultList as List<Artist>, page, (countQuery.singleResult as BigInteger).toLong())
    }
}