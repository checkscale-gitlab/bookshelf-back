package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.ContractId
import org.springframework.data.jpa.repository.JpaRepository

interface ContractRepository : JpaRepository<Contract, ContractId>
