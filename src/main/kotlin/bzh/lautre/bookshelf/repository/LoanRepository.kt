package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Loan
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface LoanRepository : JpaRepository<Loan, Long>, JpaSpecificationExecutor<Loan>