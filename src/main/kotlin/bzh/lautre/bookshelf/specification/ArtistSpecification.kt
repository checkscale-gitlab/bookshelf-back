package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Artist

class ArtistSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Artist>(criteria)

