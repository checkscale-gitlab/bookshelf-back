package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.*
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.*

class BookSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Book>(criteria) {
    companion object {
        fun orderBy(column: String, asc: Boolean = true): Specification<Book> {
            return Specification{ root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val e: Expression<*> = root.get<String>(column)
                query.orderBy(if (asc) builder.asc(e) else builder.desc(e))
                null
            }
        }

        fun orderBooks(direction: Sort.Direction, sort: BookSortField): Specification<Book> {
            return Specification{ root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val tome: Expression<*> = root.get<String>("tome")
                val joinSeries = root.join<Book, Series>("series")
                val seriesName: Expression<*> = joinSeries.get<String>("displayName")

                val orderBys = if (sort == BookSortField.EDITOR) {
                    listOf(joinSeries.join<Series, Editor>("editor").get<String>("name"), seriesName, tome)
                } else {
                    listOf(seriesName, tome)
                }

                if (direction == Sort.Direction.ASC) {
                    query.orderBy(orderBys.map { builder.asc(it) })
                } else {
                    query.orderBy(orderBys.map { builder.desc(it) })
                }
                null
            }
        }
    }

    override fun toPredicate(root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "status" ->
                build(root, criteria.operation, criteria.key, BookReadStatusEnum.valueOf(this.criteria.value!!.toString()), builder)
            "bookType" ->
                build(
                    root.join<Book, Series>("series").join<Series, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            "editor" ->
                build(
                    root.join<Book, Series>("series").join<Series, Editor>("editor"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            "series" ->
                build(
                    root.join<Book, Series>("series"),
                    this.criteria.operation, "displayName", this.criteria.value, builder
                )
            else -> super.toPredicate(root, query, builder)
        }
    }
}
