package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Series
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

class BookTypeSpecification(criteria: SpecSearchCriteria) : CommonSpecification<BookType>(criteria) {

    companion object {
        fun getAllOrderByBookCount(): Specification<BookType> {
            return Specification { root: Root<BookType>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                query.groupBy(listOf(root.get<Long>("id"), root.get("name")))
                query.orderBy(builder.desc(builder.count(root.join<BookType, Series>("seriesList").join<Series, Book>("bookList").get<String>("isbn"))))
                null
            }
        }
    }
}